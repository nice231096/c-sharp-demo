public class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Enter -1 to exit!\n");

        do
        {
            Console.Write("Enter your bottle number: ");
            string val = Console.ReadLine();

            var isNumeric = int.TryParse(val, out int num);
            if (!isNumeric)
            {
                Console.WriteLine("Invalid number\n");
                return;
            }

            if (num == -1) // exit
            {
                Console.WriteLine("Exited\n");
                return;
            }

            if (num <= 0 || num >= Math.Pow(10, 5))
            {
                Console.WriteLine("Number must be between 1 and 99999\n");
            }
            else
            {
                CalculateByDoWhile(num);
            }
        } while (true);
    }

    private static void CalculateByDoWhile(int num)
    {
        int total = num;
        int check = num;
        bool flag = true;
        do
        {
            int division = check / 3;
            int remainder = check % 3;
            if (division > 0)
            {
                total += division;
                check = division + remainder;
            }
            else
            {
                flag = false;
            }
        } while (flag);

        Console.WriteLine($"Result: {total}\n");
    }

    private static void CalculateByForLoop(int num)
    {
        int check = 1;
        for (int i = 1; i <= num; i++)
        {
            if (check % 3 == 0)
            {
                num++;
                check = 1;
            }
            else
            {
                check++;
            }
        }

        Console.WriteLine($"Result: {num}\n");
    }
}
